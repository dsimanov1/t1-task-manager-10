package ru.t1.simanov.tm.component;

import ru.t1.simanov.tm.api.*;
import ru.t1.simanov.tm.constant.ArgumentConst;
import ru.t1.simanov.tm.constant.TerminalConst;
import ru.t1.simanov.tm.controller.CommandController;
import ru.t1.simanov.tm.controller.ProjectController;
import ru.t1.simanov.tm.controller.TaskController;
import ru.t1.simanov.tm.repository.CommandRepository;
import ru.t1.simanov.tm.repository.ProjectRepository;
import ru.t1.simanov.tm.repository.TaskRepository;
import ru.t1.simanov.tm.service.CommandService;
import ru.t1.simanov.tm.service.ProjectService;
import ru.t1.simanov.tm.service.TaskService;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                commandController.showErrorCommand();
                break;
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
                break;
        }
    }

    public void run(final String[] args) {
        if (processArgument(args)) System.exit(0);
        commandController.showWelcome();
        while (true) {
            commandController.showEnter();
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void exit() {
        System.exit(0);
    }

}
