package ru.t1.simanov.tm.service;

import ru.t1.simanov.tm.api.ICommandRepository;
import ru.t1.simanov.tm.api.ICommandService;
import ru.t1.simanov.tm.model.Command;

public final class CommandService implements ICommandService {

    public final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
