package ru.t1.simanov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
