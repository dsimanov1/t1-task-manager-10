package ru.t1.simanov.tm.api;

public interface ICommandController {
    void showInfo();

    void showWelcome();

    void showEnter();

    void showErrorArgument();

    void showArguments();

    void showCommands();

    void showVersion();

    void showAbout();

    void showHelp();

    void showErrorCommand();
}
