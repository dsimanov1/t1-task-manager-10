package ru.t1.simanov.tm.api;

import ru.t1.simanov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    void clear();

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

}
