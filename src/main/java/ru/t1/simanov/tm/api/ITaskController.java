package ru.t1.simanov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
